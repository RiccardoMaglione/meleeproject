// Copyright Epic Games, Inc. All Rights Reserved.

#include "MeleeProject.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, MeleeProject, "MeleeProject" );
