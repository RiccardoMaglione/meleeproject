// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MeleeProjectGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MELEEPROJECT_API AMeleeProjectGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
