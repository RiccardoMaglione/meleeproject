// Fill out your copyright notice in the Description page of Project Settings.


#include "RM_Melee_UserWidget.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/GameUserSettings.h"

bool URM_Melee_UserWidget::Initialize()
{
	Super::Initialize();

    UGameUserSettings* TempGameUserSettings = UGameUserSettings::GetGameUserSettings();                                                                             //Get GameUserSettings
    TempGameUserSettings->LoadSettings();                                                                                                                           //Load setting of GameUserSettings
    if (TempGameUserSettings->GetFullscreenMode() == EWindowMode::Fullscreen) {                                                                                     //If is fullscreen in GameUseSettings
        TempGameUserSettings->SetFullscreenMode(EWindowMode::Fullscreen);                                                                                           //Set fullscreen
        TempGameUserSettings->SetScreenResolution(TempGameUserSettings->GetDesktopResolution());                                                                    //Set resolution of game by desktop resolution
    }
    if (TempGameUserSettings->GetFullscreenMode() == EWindowMode::Windowed || TempGameUserSettings->GetFullscreenMode() == EWindowMode::WindowedFullscreen) {       //If is windowed in GameUseSettings
        TempGameUserSettings->SetFullscreenMode(EWindowMode::Windowed);                                                                                             //Set windowed
    }
    TempGameUserSettings->ApplySettings(false);                                                                                                                     //Apply setting in GameUserSettings
    TempGameUserSettings->SaveSettings();                                                                                                                           //Save setting in GameUserSettings

	Button_Play->OnClicked.AddDynamic(this, &URM_Melee_UserWidget::PlayButtonClicked);					                                                            //Added delegate function "PlayButtonClicked" in "OnClicked" of Button_Play
	Button_Option->OnClicked.AddDynamic(this, &URM_Melee_UserWidget::OptionButtonClicked);				                                                            //Added delegate function "OptionButtonClicked" in "OnClicked" of Button_Option
	Button_Quit->OnClicked.AddDynamic(this, &URM_Melee_UserWidget::QuitButtonClicked);					                                                            //Added delegate function "QuitButtonClicked" in "OnClicked" of Button_Quit

	return true;
}

void URM_Melee_UserWidget::PlayButtonClicked()
{
	UGameplayStatics::OpenLevel(GetWorld(), "Level_MeleeGameplay");										                                                            //Change level in "Level_MeleeGameplay"
}

void URM_Melee_UserWidget::OptionButtonClicked()
{
	UGameplayStatics::OpenLevel(GetWorld(), "Level_Melee_Option");										                                                            //Change level in "Level_Melee_Option"
}

void URM_Melee_UserWidget::QuitButtonClicked()
{
	GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");										                                                            //Quit game by console command
}