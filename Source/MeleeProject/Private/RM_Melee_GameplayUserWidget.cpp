// Fill out your copyright notice in the Description page of Project Settings.


#include "RM_Melee_GameplayUserWidget.h"
#include "Components/ProgressBar.h"
#include "Kismet/GameplayStatics.h"

bool URM_Melee_GameplayUserWidget::Initialize()
{
	Super::Initialize();

	if (GetWorld()->GetFirstPlayerController() != nullptr && GetWorld()->GetFirstPlayerController()->GetPawn() != nullptr) {						//If player controller exist and pawn of player controller exist
		ARM_Melee_PlayerCharacter* CurrentRMPlayerCharacter = Cast<ARM_Melee_PlayerCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());	//Get player character and cast in "ARM_Melee_PlayerCharacter"
		if (CurrentRMPlayerCharacter != nullptr) {																									//If player character exist
			ProgressBar_PlayerLife->PercentDelegate.BindUFunction(this, "PlayerLifeView");															//Bind function at percent of progress bar of player
		}
	}

	ProgressBar_AILife->PercentDelegate.BindUFunction(this, "AILifeView");																			//Bind function at percent of progress bar of AI


	return true;
}

float URM_Melee_GameplayUserWidget::PlayerLifeView()
{
	if (GetWorld()->GetFirstPlayerController() != nullptr && GetWorld()->GetFirstPlayerController()->GetPawn() != nullptr) {						//If player controller exist and pawn of player controller exist
		ARM_Melee_PlayerCharacter* CurrentRMPlayerCharacter = Cast<ARM_Melee_PlayerCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());	//Get player character and cast in "ARM_Melee_PlayerCharacter"
		if (CurrentRMPlayerCharacter != nullptr) {																									//If player character exist
			ProgressBar_PlayerLife->Percent = (float)CurrentRMPlayerCharacter->PlayerActualLife / (float)CurrentRMPlayerCharacter->PlayerMaxLife;	//Set percent of progress bar
			return ProgressBar_PlayerLife->Percent;
		}
		else {
				return 0.0f;
		}
	}
	else {
			return 0.0f;
	}
}

float URM_Melee_GameplayUserWidget::AILifeView()
{
	URM_Melee_GameInstance* testInstance = Cast<URM_Melee_GameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));								//Get game instance and cast in "URM_Melee_GameInstance"
	if (testInstance != nullptr && testInstance->EnemyTemp != nullptr) {																			//If game instance and enemy exist
		ProgressBar_AILife->Percent = (float)testInstance->EnemyTemp->AIHealth/ (float)testInstance->EnemyTemp->AIHealthMax;						//Set percent of progress bar
	}
	return ProgressBar_AILife->Percent;
}
