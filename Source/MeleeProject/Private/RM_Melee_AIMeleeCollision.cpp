// Fill out your copyright notice in the Description page of Project Settings.


#include "RM_Melee_AIMeleeCollision.h"
#include "Components/BoxComponent.h"

// Sets default values
ARM_Melee_AIMeleeCollision::ARM_Melee_AIMeleeCollision()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	AIMeleeCollision = CreateDefaultSubobject<UBoxComponent>("Melee Collision");										//Create box collision
}

// Called when the game starts or when spawned
void ARM_Melee_AIMeleeCollision::BeginPlay()
{
	Super::BeginPlay();

	AIMeleeCollision->OnComponentBeginOverlap.AddDynamic(this, &ARM_Melee_AIMeleeCollision::HandleAIMeleeOverlap);		//Add delegate function "HandleAIMeleeOverlap" in "OnComponentBeginOverlap"
}

void ARM_Melee_AIMeleeCollision::HandleAIMeleeOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherComp->ComponentHasTag("Player")) {																			//If tag is player of other comp
		ARM_Melee_PlayerCharacter* temp = Cast<ARM_Melee_PlayerCharacter>(OtherComp->GetOwner());						//Get player character
		temp->PlayerActualLife -= 1;																					//Decrease player life
	}
}

// Called every frame
void ARM_Melee_AIMeleeCollision::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}