// Fill out your copyright notice in the Description page of Project Settings.


#include "RM_Melee_GameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "Components/AudioComponent.h"
#include "Components/Slider.h"

void URM_Melee_GameInstance::PlayAudioBtwScene(USoundBase* Sound, float VolumeMultiplier, bool isPersist) {
	if (AudioComp == nullptr) {																									//If audio comp don't exist
		AudioComp = UGameplayStatics::CreateSound2D(GetWorld(), Sound, VolumeMultiplier, 1.0f, 0.0f, nullptr, isPersist);		//Create audio
		AudioComp->Play();																										//Play audio
	}
}

void URM_Melee_GameInstance::StopAudioBtwScene()
{
	if (AudioComp != nullptr) {																									//If audio comp exist
		AudioComp->Stop();																										//Stop audio
	}
}