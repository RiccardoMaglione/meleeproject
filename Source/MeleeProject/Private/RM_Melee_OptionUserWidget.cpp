// Fill out your copyright notice in the Description page of Project Settings.


#include "RM_Melee_OptionUserWidget.h"
#include "Components/CheckBox.h"
#include "Components/Button.h"
#include "Components/Slider.h"
#include "Components/TextBlock.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/GameUserSettings.h"

bool URM_Melee_OptionUserWidget::Initialize()
{
    Super::Initialize();

    UGameUserSettings* TempGameUserSettings = UGameUserSettings::GetGameUserSettings();                                                                                         //Get GameUserSettings
    TempGameUserSettings->LoadSettings();                                                                                                                                       //Load setting of GameUserSettings
    if (TempGameUserSettings->GetFullscreenMode() == EWindowMode::Fullscreen) {                                                                                                 //If is fullscreen in GameUseSettings
        CheckBox_Fullscreen->CheckedState = ECheckBoxState::Checked;                                                                                                            //Set CheckedState in Checked
        TempGameUserSettings->SetScreenResolution(TempGameUserSettings->GetDesktopResolution());                                                                                //Set resolution of game by desktop resolution
        TempGameUserSettings->ApplySettings(false);                                                                                                                             //Apply setting in GameUserSettings
        TempGameUserSettings->SaveSettings();                                                                                                                                   //Save setting in GameUserSettings
    }
    if (TempGameUserSettings->GetFullscreenMode() == EWindowMode::Windowed || TempGameUserSettings->GetFullscreenMode() == EWindowMode::WindowedFullscreen) {                   //If is Windowed or WindowedFullscreen in GameUseSettings
        CheckBox_Fullscreen->CheckedState = ECheckBoxState::Unchecked;                                                                                                          //Set CheckedState in Unchecked
    }

    CheckBox_Fullscreen->OnCheckStateChanged.AddDynamic(this, &URM_Melee_OptionUserWidget::FullscreenCheckBoxClicked);                                                          //Added delegate function "FullscreenCheckBoxClicked" in "OnCehckStateChanged" of check box
    Button_Back->OnClicked.AddDynamic(this, &URM_Melee_OptionUserWidget::BackButtonClicked);                                                                                    //Added delegate function "BackButtonClicked" in "OnClicker" of button

    URM_Melee_GameInstance* testInstance = Cast<URM_Melee_GameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));                                                         //Get game instance and cast in "URM_Melee_GameInstance"
    if (testInstance != nullptr) {                                                                                                                                              //If game instance exist
        Slider_Volume->Value = testInstance->VolumeTemp;                                                                                                                        //Set slider value
    }

    float tempValue = Slider_Volume->Value;
    tempValue *= 100;
    FString temp = FString::SanitizeFloat((int)tempValue);                                                                                                                      //Convert cast<float>int in string
    FString tempchar = "%";
    temp.Append(tempchar);                                                                                                                                                      //Added in tempValue temp for create string of value volume
    TextBlock_VolumeValue->SetText(FText::FromString(temp));                                                                                                                    //Set text block of value volume

    Master_Volume = Slider_Volume->Value;                                                                                                                                       //Set master volume
    UGameplayStatics::SetSoundMixClassOverride(GetWorld(), Master_InSoundMix, Master_InSoundClass, Master_Volume, Master_Pitch, Master_FadeInTime, Master_bApplyToChildren);    //Set sound mix class
    UGameplayStatics::PushSoundMixModifier(GetWorld(), Master_InSoundMix);                                                                                                      //Push modifier in Master_InSoundMix
    Slider_Volume->OnValueChanged.AddDynamic(this, &URM_Melee_OptionUserWidget::SliderVolumeChangedBind);                                                                       //Added delegate function "SliderVolumeChangedBind" in "OnValueChanged" of slider

    return true;
}

void URM_Melee_OptionUserWidget::FullscreenCheckBoxClicked(bool isChecked)
{
    GetWorld()->GetFirstPlayerController()->ConsoleCommand("fullscreen");                                                                                                       //Set fullscreen by console command
    UGameUserSettings* TempGameUserSettings = UGameUserSettings::GetGameUserSettings();                                                                                         //Get GameUserSettings
    if (CheckBox_Fullscreen->CheckedState == ECheckBoxState::Checked) {                                                                                                         //If CheckedState is Checked
        TempGameUserSettings->SetFullscreenMode(EWindowMode::Fullscreen);                                                                                                       //Set fullscreen
        TempGameUserSettings->SetScreenResolution(TempGameUserSettings->GetDesktopResolution());                                                                                //Set resolution of game by desktop resolution
    }
    if (CheckBox_Fullscreen->CheckedState == ECheckBoxState::Unchecked) {                                                                                                       //If CheckedState is Unchecked
        TempGameUserSettings->SetFullscreenMode(EWindowMode::Windowed);                                                                                                         //Set windowed
    }
    TempGameUserSettings->ApplySettings(false);                                                                                                                                 //Apply setting in GameUserSettings
    TempGameUserSettings->SaveSettings();                                                                                                                                       //Save setting in GameUserSettings
}

void URM_Melee_OptionUserWidget::BackButtonClicked()
{
    UGameplayStatics::OpenLevel(GetWorld(), "Level_Melee_Menu");                                                                                                                //Change level in "Level_Melee_Menu"
}

void URM_Melee_OptionUserWidget::SliderVolumeChangedBind(float Value)
{

    float tempValue = Value;
    tempValue *= 100;
    FString temp = FString::SanitizeFloat((int)tempValue);                                                                                                                      //Convert cast<float>int in string
    FString tempchar = "%";
    temp.Append(tempchar);                                                                                                                                                      //Added in tempValue temp for create string of value volume
    TextBlock_VolumeValue->SetText(FText::FromString(temp));                                                                                                                    //Set text block of value volume

    Master_Volume = Value;
    UGameplayStatics::SetSoundMixClassOverride(GetWorld(), Master_InSoundMix, Master_InSoundClass, Master_Volume, Master_Pitch, Master_FadeInTime, Master_bApplyToChildren);    //Set sound mix class
    UGameplayStatics::PushSoundMixModifier(GetWorld(), Master_InSoundMix);                                                                                                      //Push modifier in Master_InSoundMix


    URM_Melee_GameInstance* testInstance = Cast<URM_Melee_GameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));                                                         //Get game instance and cast in "URM_Melee_GameInstance"
    testInstance->VolumeTemp = Value;                                                                                                                                           //Set volume temp in game instance
}