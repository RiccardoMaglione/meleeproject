// Fill out your copyright notice in the Description page of Project Settings.

#include "RM_Melee_PlayerAnimInstance.h"
#include "GameFramework\CharacterMovementComponent.h"

void URM_Melee_PlayerAnimInstance::NativeInitializeAnimation() {

	Super::NativeInitializeAnimation();
}


void URM_Melee_PlayerAnimInstance::NativeUpdateAnimation(float DeltaSeconds) {
	Super::NativeUpdateAnimation(DeltaSeconds);

	const APawn* pawnOwner = TryGetPawnOwner();																				//Try to get pawn of AnimInstance

	if (pawnOwner != nullptr) {
		const UCharacterMovementComponent* charMov = pawnOwner->FindComponentByClass<UCharacterMovementComponent>();		//Get character movement
		if (charMov) {
			const FVector speed = pawnOwner->GetVelocity();																	//Get velocity of character movement
			const float maxSpeed = charMov->GetMaxSpeed();																	//Get max speed of character movement
			const float currentSpeed = speed.Size();																		//Get magnitude of speed
			NormalizedSpeed = currentSpeed / maxSpeed;																		//Calculate normalize speed
			bIsMoving = !FMath::IsNearlyZero(NormalizedSpeed);																//Set bool if normalize speed is near 0

			bIsFalling = charMov->IsFalling();																				//Set bIsFalling by character movement

			const FVector velocity = pawnOwner->GetVelocity();																//Get velocity of character movement of pawn
			const FRotator rotation = pawnOwner->GetActorRotation();														//Get actor rotation of character movement of pawn
			Direction = CalculateDirection(velocity, rotation);																//Calculate and set direction
		}
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("Missing Pawn Owner!"));
	}

}