// Fill out your copyright notice in the Description page of Project Settings.


#include "RM_Melee_PauseUserWidget.h"
#include "Kismet/GameplayStatics.h"
#include "Components/Button.h"

bool URM_Melee_PauseUserWidget::Initialize()
{
    Super::Initialize();

    Button_ReturnToMenu->OnClicked.AddDynamic(this, &URM_Melee_PauseUserWidget::ReturnToMenuButtonClicked);             //Added delegate function "ReturnToMenuButtonClicked" in button OnClicked
    Button_Quit->OnClicked.AddDynamic(this, &URM_Melee_PauseUserWidget::QuitButtonClicked);                             //Added delegate function "QuitButtonClicked" in button OnClicked

    return true;
}

void URM_Melee_PauseUserWidget::ReturnToMenuButtonClicked()
{
    UGameplayStatics::OpenLevel(GetWorld(), "Level_Melee_Menu");                                                        //Change level in "Level_Melee_Menu"
}

void URM_Melee_PauseUserWidget::QuitButtonClicked()
{
    GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");                                                     //Quit of game by console command
}
