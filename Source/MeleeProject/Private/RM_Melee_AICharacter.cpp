// Fill out your copyright notice in the Description page of Project Settings.


#include "RM_Melee_AICharacter.h"

// Sets default values
ARM_Melee_AICharacter::ARM_Melee_AICharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ARM_Melee_AICharacter::BeginPlay()
{
	Super::BeginPlay();
	AIHealth = AIHealthMax;																					//Set ai health
}

// Called every frame
void ARM_Melee_AICharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (AIHealth <= 0 && AIIsDeath == false) {
		AIIsDeath = true;																					//Set bool for death
	}
}

// Called to bind functionality to input
void ARM_Melee_AICharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}