// Fill out your copyright notice in the Description page of Project Settings.


#include "RM_Melee_AIAnimInstance.h"
#include "GameFramework\CharacterMovementComponent.h"
#include "RM_Melee_AICharacter.h"

void URM_Melee_AIAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();
}

void URM_Melee_AIAnimInstance::NativeUpdateAnimation(float DeltaTime)
{
	Super::NativeUpdateAnimation(DeltaTime);

	const APawn* pawnOwner = TryGetPawnOwner();

	if (pawnOwner != nullptr) {
		const UCharacterMovementComponent* charMov = pawnOwner->FindComponentByClass<UCharacterMovementComponent>();		//Get character movement
		if (charMov) {
			const FVector speed = pawnOwner->GetVelocity();																	//Get velocity of character movement
			if (speed == FVector(0.0f, 0.0f, 0.0f)) {
				bCanMove = false;
			}
			else {
				bCanMove = true;
			}
			const ARM_Melee_AICharacter* tempchar = Cast<ARM_Melee_AICharacter>(pawnOwner);
			bAIIsDeath = tempchar->AIIsDeath;
		}
	}
}