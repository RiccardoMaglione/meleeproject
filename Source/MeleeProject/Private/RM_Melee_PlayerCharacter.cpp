// Fill out your copyright notice in the Description page of Project Settings.


#include "RM_Melee_PlayerCharacter.h"
#include "Kismet/KismetMathLibrary.h"
#include "GameFramework/Character.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
ARM_Melee_PlayerCharacter::ARM_Melee_PlayerCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>("Melee Spring Arm");						//Create inherited spring arm 
	Camera = CreateDefaultSubobject<UCameraComponent>("Melee Camera");									//Create inherited camera 
	SpringArm->SetupAttachment(RootComponent);															//Set parent of spring arm at RootComponent
	Camera->SetupAttachment(SpringArm);																	//Set parent of camera at spring arm
}

// Called when the game starts or when spawned
void ARM_Melee_PlayerCharacter::BeginPlay()
{
	Super::BeginPlay();

	PlayerActualLife = PlayerMaxLife;																	//Set Player actual life
}

void ARM_Melee_PlayerCharacter::MovementForward(const float InputValue)
{
	const FRotator PlayeRotation = GetControlRotation();												//Get rotation of player
	const FRotator PlayerDirection = FRotator(0.0f, PlayeRotation.Yaw, 0.0f);							//Set rotation of direction of player
	const FVector PlayerDirectionVector = UKismetMathLibrary::GetForwardVector(PlayerDirection);		//Get forward vector of direction
	UCharacterMovementComponent* movementComponent = GetCharacterMovement();							//Get character movement
	if (movementComponent) {
		movementComponent->AddInputVector(PlayerDirectionVector * InputValue, false);					//Move player
	}
}

void ARM_Melee_PlayerCharacter::MovementRight(const float InputValue)
{
	const FRotator PlayeRotation = GetControlRotation();												//Get rotation of player
	const FRotator PlayerDirection = FRotator(0.0f, PlayeRotation.Yaw, 0.0f);							//Set rotation of direction of player
	const FVector PlayerDirectionVector = UKismetMathLibrary::GetRightVector(PlayerDirection);			//Get right vector of direction
	UCharacterMovementComponent* movementComponent = GetCharacterMovement();							//Get character movement
	if (movementComponent) {
		movementComponent->AddInputVector(PlayerDirectionVector * InputValue, false);					//Move player
	}
}

// Called every frame
void ARM_Melee_PlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ARM_Melee_PlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}