// Fill out your copyright notice in the Description page of Project Settings.


#include "RM_Melee_GameOverUserWidget.h"
#include "Kismet/GameplayStatics.h"
#include "Components/Button.h"

bool URM_Melee_GameOverUserWidget::Initialize()
{
	Super::Initialize();
	Button_ReturnToMenu->OnClicked.AddDynamic(this, &URM_Melee_GameOverUserWidget::ReturnToMenuButtonClicked);			//Added delegate function "ReturnToMenuButtonClicked" in button OnClicked
	Button_Quit->OnClicked.AddDynamic(this, &URM_Melee_GameOverUserWidget::QuitButtonClicked);							//Added delegate function "QuitButtonClicked" in button OnClicked
	return true;
}

void URM_Melee_GameOverUserWidget::ReturnToMenuButtonClicked()
{
	UGameplayStatics::OpenLevel(GetWorld(), "Level_Melee_Menu");														//Change level in "Level_Melee_Menu"
}

void URM_Melee_GameOverUserWidget::QuitButtonClicked()
{
	GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");														//Quit of game by console command
}
