// Fill out your copyright notice in the Description page of Project Settings.

#include "RM_Melee_BTTaskNode_AI_Move.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"

EBTNodeResult::Type URM_Melee_BTTaskNode_AI_Move::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{

	ARM_Melee_AICharacter* AIMeleeCharacter = Cast<ARM_Melee_AICharacter>(OwnerComp.GetAIOwner()->GetPawn());																		//Get AI Pawn by owner
	if (AIMeleeCharacter->AIIsDeath == false) {
		OwnerComp.GetAIOwner()->MoveToActor(GetWorld()->GetFirstPlayerController()->GetPawn(), 50);																					//Move AI Pawn to player location
		FRotator PlayerRot = UKismetMathLibrary::FindLookAtRotation(AIMeleeCharacter->GetActorLocation(), GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorLocation());	//Set look at
		PlayerRot.Pitch = 0;																																						//Set pitch Y axis at 0
		AIMeleeCharacter->SetActorRotation(PlayerRot);																																//Set rotation of AI
	}

	return EBTNodeResult::Type();
}
