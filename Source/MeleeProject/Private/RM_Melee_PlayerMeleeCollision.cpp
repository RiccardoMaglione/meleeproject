// Fill out your copyright notice in the Description page of Project Settings.


#include "RM_Melee_PlayerMeleeCollision.h"
#include "Components/BoxComponent.h"

// Sets default values
ARM_Melee_PlayerMeleeCollision::ARM_Melee_PlayerMeleeCollision()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PlayerMeleeCollision = CreateDefaultSubobject<UBoxComponent>("Melee Collision");															//Create box collision
}

// Called when the game starts or when spawned
void ARM_Melee_PlayerMeleeCollision::BeginPlay()
{
	Super::BeginPlay();

	PlayerMeleeCollision->OnComponentBeginOverlap.AddDynamic(this, &ARM_Melee_PlayerMeleeCollision::HandlePlayerMeleeOverlap);					//Added delegate function "HandlePlayerMeleeOverlap" in "OnComponentBeginOverlap"
	
}

void ARM_Melee_PlayerMeleeCollision::HandlePlayerMeleeOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherComp->ComponentHasTag("Enemy")) {																									//If tag of other comp is enemy
		ARM_Melee_AICharacter* temp = Cast<ARM_Melee_AICharacter>(OtherComp->GetOwner());														//Cast other comp owner and set temp
		SetEnemy(temp);																															//Call function set enemy
		temp->AIHealth -= 1;																													//Decrease health of AI
	}
}

// Called every frame
void ARM_Melee_PlayerMeleeCollision::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ARM_Melee_PlayerMeleeCollision::SetEnemy(ARM_Melee_AICharacter* Enemy)
{
	URM_Melee_GameInstance* testInstance = Cast<URM_Melee_GameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));							//Get game instance and cast in "URM_Melee_GameInstance"
	testInstance->EnemyTemp = Enemy;																											//Set enemy temp of game instance
}
