// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "RM_Melee_AIController.h"
#include "RM_Melee_AICharacter.h"
#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "RM_Melee_BTTaskNode_AI_Move.generated.h"

/**
 * 
 */
UCLASS()
class MELEEPROJECT_API URM_Melee_BTTaskNode_AI_Move : public UBTTaskNode
{
	GENERATED_BODY()
	
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
