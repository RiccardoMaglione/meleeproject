// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "RM_Melee_PlayerAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class MELEEPROJECT_API URM_Melee_PlayerAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
	

protected:

	virtual void NativeInitializeAnimation() override;

	virtual void NativeUpdateAnimation(float DeltaTime) override;

	UPROPERTY(BlueprintReadOnly, Category = "RM")
		bool bIsFalling;

	UPROPERTY(BlueprintReadOnly, Category = "RM")
		bool bIsMoving;
	UPROPERTY(BlueprintReadOnly, Category = "RM")
		float Direction;
	UPROPERTY(BlueprintReadOnly, Category = "RM")
		float NormalizedSpeed;
};
