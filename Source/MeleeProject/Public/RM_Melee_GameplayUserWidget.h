// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "RM_Melee_AIController.h"
#include "RM_Melee_PlayerMeleeCollision.h"
#include "RM_Melee_AICharacter.h"
#include "RM_Melee_PlayerCharacter.h"
#include "RM_Melee_GameMode.h"
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "RM_Melee_GameplayUserWidget.generated.h"

/**
 * 
 */
UCLASS()
class MELEEPROJECT_API URM_Melee_GameplayUserWidget : public UUserWidget
{
	GENERATED_BODY()
		virtual bool Initialize();

	UPROPERTY(meta = (BindWidget))
		class UProgressBar* ProgressBar_PlayerLife;
	UPROPERTY(meta = (BindWidget))
		class UProgressBar* ProgressBar_AILife;

	UFUNCTION(BlueprintPure)
		float PlayerLifeView();
	UFUNCTION()
		float AILifeView();
};
