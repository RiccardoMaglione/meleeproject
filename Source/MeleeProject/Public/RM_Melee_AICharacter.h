// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "RM_Melee_AICharacter.generated.h"

UCLASS()
class MELEEPROJECT_API ARM_Melee_AICharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ARM_Melee_AICharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RM | Melee | AI")
		int AIHealthMax = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RM | Melee | AI")
		int AIHealth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RM | Melee | AI")
		bool AIIsDeath;
};
