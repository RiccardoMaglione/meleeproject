// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "RM_Melee_PauseUserWidget.generated.h"

/**
 * 
 */
UCLASS()
class MELEEPROJECT_API URM_Melee_PauseUserWidget : public UUserWidget
{
	GENERATED_BODY()
	
	virtual bool Initialize();

	UPROPERTY(meta = (BindWidget))
		class UButton* Button_ReturnToMenu;
	UPROPERTY(meta = (BindWidget))
		class UButton* Button_Quit;

	UFUNCTION()
		void ReturnToMenuButtonClicked();
	UFUNCTION()
		void QuitButtonClicked();
};
