// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "RM_Melee_GameInstance.h"
#include "AudioDevice.h"
#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "RM_Melee_OptionUserWidget.generated.h"

/**
 * 
 */
UCLASS()
class MELEEPROJECT_API URM_Melee_OptionUserWidget : public UUserWidget
{
	GENERATED_BODY()
	
	virtual bool Initialize();
	
	UPROPERTY(meta = (BindWidget))
		class UCheckBox* CheckBox_Fullscreen;
	UPROPERTY(meta = (BindWidget))
		class UButton* Button_Back;
	UPROPERTY(meta = (BindWidget))
		class USlider* Slider_Volume;
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* TextBlock_VolumeValue;

	UFUNCTION()
		void FullscreenCheckBoxClicked(bool isChecked);
	UFUNCTION()
		void BackButtonClicked();
	UFUNCTION()
		void SliderVolumeChangedBind(float Value);


public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RM | Melee | Sound")
		class USoundMix* Master_InSoundMix;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RM | Melee | Sound")
		class USoundClass* Master_InSoundClass;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RM | Melee | Sound")
		float Master_Volume;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RM | Melee | Sound")
		float Master_Pitch;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RM | Melee | Sound")
		float Master_FadeInTime;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RM | Melee | Sound")
		bool Master_bApplyToChildren;
};