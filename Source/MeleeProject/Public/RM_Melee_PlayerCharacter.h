// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/Character.h"
#include "GameFramework/SpringArmComponent.h"
#include "RM_Melee_PlayerCharacter.generated.h"

UCLASS()
class MELEEPROJECT_API ARM_Melee_PlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ARM_Melee_PlayerCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//Camera Variables
	UPROPERTY(VisibleAnywhere, Category = "RM")
		USpringArmComponent* SpringArm;
	UPROPERTY(VisibleAnywhere, Category = "RM")
		UCameraComponent* Camera;
	
	//Movement Function
	UFUNCTION(BlueprintCallable, Category = "RM | Melee")
		void MovementForward(const float InputValue);
	UFUNCTION(BlueprintCallable, Category = "RM | Melee")
		void MovementRight(const float InputValue);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//Life Variables
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RM | Melee")
		int PlayerMaxLife = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RM | Melee")
		int PlayerActualLife;

	//Damage Variables
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RM | Melee")
		int PlayerDamage;
};
