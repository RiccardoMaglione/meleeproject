// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "RM_Melee_PlayerCharacter.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RM_Melee_AIMeleeCollision.generated.h"

UCLASS()
class MELEEPROJECT_API ARM_Melee_AIMeleeCollision : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARM_Melee_AIMeleeCollision();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "RM")
		class UBoxComponent* AIMeleeCollision;

	UFUNCTION()
		void HandleAIMeleeOverlap(
			UPrimitiveComponent* OverlappedComponent,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex,
			bool bFromSweep,
			const FHitResult& SweepResult);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
