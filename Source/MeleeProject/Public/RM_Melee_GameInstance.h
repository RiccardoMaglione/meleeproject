// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "RM_Melee_AICharacter.h"
#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "RM_Melee_GameInstance.generated.h"

/**
 * 
 */
UCLASS()
class MELEEPROJECT_API URM_Melee_GameInstance : public UGameInstance
{
	GENERATED_BODY()

	UPROPERTY()
		UAudioComponent* AudioComp;

	UFUNCTION(BlueprintCallable, Category = "RM | Melee | GameInstance")
		void PlayAudioBtwScene(USoundBase* Sound, float VolumeMultiplier, bool isPersist);

	UFUNCTION(BlueprintCallable, Category = "RM | Melee | GameInstance")
		void StopAudioBtwScene();

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RM | Melee | Sound")
		float VolumeTemp = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RM | Melee | AI")
		ARM_Melee_AICharacter* EnemyTemp;
};
