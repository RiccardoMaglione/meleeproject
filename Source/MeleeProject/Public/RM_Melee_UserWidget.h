// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "RM_Melee_UserWidget.generated.h"

/**
 * 
 */
UCLASS()
class MELEEPROJECT_API URM_Melee_UserWidget : public UUserWidget
{
	GENERATED_BODY()

	virtual bool Initialize();

	UPROPERTY(meta = (BindWidget))
		class UButton* Button_Play;
	UPROPERTY(meta = (BindWidget))
		class UButton* Button_Option;
	UPROPERTY(meta = (BindWidget))
		class UButton* Button_Quit;
	
	UFUNCTION()
		void PlayButtonClicked();
	UFUNCTION()
		void OptionButtonClicked();
	UFUNCTION()
		void QuitButtonClicked();
};
