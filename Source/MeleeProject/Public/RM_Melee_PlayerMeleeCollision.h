// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/GameplayStatics.h"
#include "RM_Melee_GameInstance.h"
#include "RM_Melee_AICharacter.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RM_Melee_PlayerMeleeCollision.generated.h"

UCLASS()
class MELEEPROJECT_API ARM_Melee_PlayerMeleeCollision : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARM_Melee_PlayerMeleeCollision();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "RM")
		class UBoxComponent* PlayerMeleeCollision;


	UFUNCTION()
		void HandlePlayerMeleeOverlap(
			UPrimitiveComponent* OverlappedComponent,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex,
			bool bFromSweep,
			const FHitResult& SweepResult);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "RM")
		void SetEnemy(ARM_Melee_AICharacter* Enemy);
};
