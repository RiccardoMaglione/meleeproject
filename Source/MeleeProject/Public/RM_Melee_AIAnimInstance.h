// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "RM_Melee_AIAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class MELEEPROJECT_API URM_Melee_AIAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
	
protected:

	virtual void NativeInitializeAnimation() override;

	virtual void NativeUpdateAnimation(float DeltaTime) override;


	UPROPERTY(BlueprintReadOnly, Category = "RM")
		bool bCanMove;
	UPROPERTY(BlueprintReadOnly, Category = "RM")
		bool bAIIsDeath;
};
